# Computer-Organization-Symbolic-Names

File with easy to remember names for everything used in the Computer Organization I/O unit. It also includes the original `inter.inc` given by the teacher.

### How to use?

Add at the beginning of your assembly code the next line.
```arm
.include "Jinter.s"
```
