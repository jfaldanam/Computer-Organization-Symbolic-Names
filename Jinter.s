        @Symbolic names for Computer Organization.
		
		@Turn on/off(High/Low)
	.set    TON,   		0x3F20001C
    	.set    TOFF,   	0x3F200028
		
		@All components of the daughter card.
		
		
        .set    BUZZ,    	0x010
	
		@For any combination of leds, just add their values.
	.set	RLED1,	 	0x200
	.set	RLED2,	 	0x400 
	.set	YLED1,	 	0x800
	.set	YLED2,	 	0x20000
	.set	GLED1,	 	0x400000
	.set	GLED2,      	0x8000000
	.set	ALLLEDS, 	0x08420E00
	
		@Buttons 
	.set	BUTTONS, 	0x3F200034 
	
		@Masks for buttons 1 and 2
	.set	BTN1,	 	0b0100
	.set	BTN2,	 	0b01000
	
		@Clock
	.set CTRCLO,	 	0x3F003004



		@Musical Notes
	.set    DO,     	1911
	.set    RE,   	  	1703
	.set    MI,   	    	1517
	.set    FA,    	   	1432
	.set    SOL,    	1275
	.set    LA,     	1136
	.set    SI,      	1012
	.set    DOAGUDO, 	955
		
		
		

	@Original inter.inc file.

	.macro    ADDEXC  vector, dirRTI
		ldr     r1, =(\dirRTI-\vector+0xa7fffffb)
		ror     r1, #2
		str     r1, [r0, #\vector]
	.endm
	
	.set    GPBASE,   0x3f200000
	.set    GPFSEL0,        0x00
	.set    GPFSEL1,        0x04
	.set    GPFSEL2,        0x08
	.set    GPFSEL3,        0x0c
	.set    GPFSEL4,        0x10
	.set    GPFSEL5,        0x14
	.set    GPFSEL6,        0x18
	.set    GPSET0,         0x1c
	.set    GPSET1,         0x20
	.set    GPCLR0,         0x28
	.set    GPCLR1,         0x2c
	.set    GPLEV0,         0x34
	.set    GPLEV1,         0x38
	.set    GPEDS0,         0x40
	.set    GPEDS1,         0x44
	.set    GPFEN0,         0x58
	.set    GPFEN1,         0x5c
	.set    GPPUD,          0x94
	.set    GPPUDCLK0,      0x98
	.set    STBASE,   0x3f003000
	.set    STCS,           0x00
	.set    STCLO,          0x04
	.set    STC1,           0x10
	.set    STC3,           0x18
	.set    INTBASE,  0x3f00b000
	.set    INTFIQCON,     0x20c
	.set    INTENIRQ1,     0x210
	.set    INTENIRQ2,     0x214
